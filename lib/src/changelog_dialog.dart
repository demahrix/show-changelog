import 'package:flutter/material.dart';
import 'package:show_changelog/src/changelog_model.dart';

Future<void> showChangelogDialog({
  BuildContext context,
  List<ChangelogModel> data
}) {
  return showDialog(context: context, builder: (_) => _ChangelogDialogWidget(data));
}

class _ChangelogDialogWidget extends StatelessWidget {

  final List<ChangelogModel> _data;
  _ChangelogDialogWidget(this._data);

  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;

    return Dialog(
      child: Container(
        width: size.width * 0.45,
        height: size.height * 0.8,
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                itemCount: _data.length,
                itemBuilder: (_, index) => _ChangelogDialogItemWidget(_data[index]),
              ),
            ),
            Divider(height: 1.0,),
            Container(
              height: 54.0,
              alignment: Alignment.centerRight,
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: TextButton(
                onPressed: Navigator.of(context, rootNavigator: true).pop,
                child: Text("Fermer"),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _ChangelogDialogItemWidget extends StatelessWidget {

  final ChangelogModel _data;
  const _ChangelogDialogItemWidget(this._data);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          const SizedBox(height: 20.0),

          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                _data.version,
                style: TextStyle(
                  fontSize: 28.0,
                  fontWeight: FontWeight.w600
                ),
              ),

              const SizedBox(width: 10.0,),

              Text(
                _data.date,
                style: TextStyle(
                  color: Colors.grey
                ),
              )
            ],
          ),

          for (int i=0, len = _data.contents.length; i<len; ++i)
            ..._buildContent(_data.contents[i])

        ],
      ),
    );
  }

  static List<Widget> _buildContent(ChangelogContentsModel content) {
    final items = content.items;
    return [

      const SizedBox(height: 30.0),

      Text(
        content.title,
        style: TextStyle(
          fontSize: 20.0,
          fontWeight: FontWeight.w600
        ),
      ),

      Padding(
        padding: EdgeInsets.only(top: 5.0, left: 15.0, right: 15.0),
        child: Column(
          children: List.generate(items.length, (index) => Row(
            children: [
              // const Text(
              //   "• ",
              //   style: TextStyle(
              //     fontSize: 26.0
              //   )
              // ),
              Container( // creer un composant constant
                width: 7.0,
                height: 7.0,
                decoration: BoxDecoration(
                  color: Colors.black87,
                  shape: BoxShape.circle
                ),
              ),
              const SizedBox(width: 10.0),
              Text(items[index])
            ],
          )),
        ),
      )

    ];
  }


}

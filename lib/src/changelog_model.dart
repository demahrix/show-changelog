
class ChangelogContentsModel {

  /// titre de la modification []
  final String title;
  final List<String> items; // string pour l'instant mais peut etre beaucoup plus

  ChangelogContentsModel(this.title, this.items);

  factory ChangelogContentsModel.fromJson(Map<String, dynamic> data) {
    throw UnimplementedError();
  }

}

class ChangelogModel {

  final String version; // la version de l'application (ou le titre suivi de la version)

  final String date; // la date de la version

  final List<ChangelogContentsModel> contents;

  ChangelogModel(this.version, this.date, this.contents);

  factory ChangelogModel.fromJson(Map<String, dynamic> data) {
    throw UnimplementedError();
  }

}
